# Mole Rat Robotics

Author: Tyler Quist, Zac Lou, Khoa Nguyen
Final project for Robotics Course. Code is executable using Webots.

## About
- Our attempt to make a robot that traverse through a premade set of mazes.
- The robot uses simple trick to stick to either the left or the right wall to find its way out of the maze.
- Can navigate on both straight and curved maze.